**GitHub Protobuf**

-- Projeto:
https://github.com/protocolbuffers/protobuf/releases/tag/v3.8.0

-- Artigo:
https://medium.com/trainingcenter/protobuf-uma-alternativa-ao-json-e-xml-a35c66edab4d

-- Documentação Google:
https://developers.google.com/protocol-buffers
-- Documentação para Python:
https://developers.google.com/protocol-buffers/docs/pythontutorial
